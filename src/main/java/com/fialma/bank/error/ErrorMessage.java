package com.fialma.bank.error;

import org.springframework.http.HttpStatus;

public record ErrorMessage(HttpStatus status, String message) {}