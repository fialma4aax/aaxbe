package com.fialma.bank.service;

import com.fialma.bank.repository.TransactionRepository;
import com.fialma.bank.entity.Account;
import com.fialma.bank.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService{
    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public List<Transaction> findByAccount(Account account) {
        return transactionRepository.findByAccountId(account.getId());
    }
}
