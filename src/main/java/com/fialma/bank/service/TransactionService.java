package com.fialma.bank.service;

import com.fialma.bank.entity.Account;
import com.fialma.bank.entity.Transaction;

import java.util.List;

public interface TransactionService {
    List<Transaction> findByAccount(Account account);
}
